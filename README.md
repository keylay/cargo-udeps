<!--
SPDX-FileCopyrightText: 2023 John Moon <john@jmoon.dev>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Cargo Udeps

This project generates a build container for the Keylay project that simply
runs [`cargo udeps`](https://crates.io/crates/cargo-udeps).

`cargo udeps` requires the nightly compiler and requires some time to install,
so it's easiest to just pre-package a container with the tool.
